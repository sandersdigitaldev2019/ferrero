$mobile = 812;
$site = window.location.origin;

$('.helperComplement ').remove();

var header = {
	'Accept': 'application/json',
	'REST-range': 'resources=0-1000',
	'Content-Type': 'application/json; charset=utf-8'
};

var insertMasterData = function (ENT, loja, dados, fn) {
	$.ajax({
		url: '/' + loja + '/dataentities/' + ENT + '/documents/',
		type: 'PATCH',
		data: dados,
		headers: header,
		success: function (res) {
			fn(res);
		},
		error: function (res) {
			swal("Oops!", "Houve um problema. Tente novamente mais tarde.", "error");
		}
	});
};

var qtd = function () {
	//TOTAL CARRINHO
	vtexjs.checkout.getOrderForm()
		.done(function (orderForm) {
			var quantidade = 0;
			for (var i = orderForm.items.length - 1; i >= 0; i--) {
				quantidade = parseInt(quantidade) + parseInt(orderForm.items[i].quantity);
			}

			$('header #qtd').text(quantidade);
		});
}

var open_modal = function (value) {
	//REMOVE ALL
	$('div.modal.active').removeClass('active');

	$(value).addClass('active');
	$('#overlay').addClass('active');
};

var set_cookie = function (cname, cvalue, exdays) {
	var d = new Date();
	d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
	var expires = "expires=" + d.toUTCString();
	document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
};

var get_cookie = function (cname) {
	var name = cname + "=";
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
};

var validate_cnpj = function (cnpj) {
	cnpj = cnpj.replace(/[^\d]+/g, '');

	if (cnpj == '') return false;

	if (cnpj.length != 14)
		return false;

	// Elimina CNPJs invalidos conhecidos
	if (cnpj == "00000000000000" ||
		cnpj == "11111111111111" ||
		cnpj == "22222222222222" ||
		cnpj == "33333333333333" ||
		cnpj == "44444444444444" ||
		cnpj == "55555555555555" ||
		cnpj == "66666666666666" ||
		cnpj == "77777777777777" ||
		cnpj == "88888888888888" ||
		cnpj == "99999999999999")
		return false;

	// Valida DVs
	tamanho = cnpj.length - 2
	numeros = cnpj.substring(0, tamanho);
	digitos = cnpj.substring(tamanho);
	soma = 0;
	pos = tamanho - 7;
	for (i = tamanho; i >= 1; i--) {
		soma += numeros.charAt(tamanho - i) * pos--;
		if (pos < 2)
			pos = 9;
	}
	resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
	if (resultado != digitos.charAt(0))
		return false;

	tamanho = tamanho + 1;
	numeros = cnpj.substring(0, tamanho);
	soma = 0;
	pos = tamanho - 7;
	for (i = tamanho; i >= 1; i--) {
		soma += numeros.charAt(tamanho - i) * pos--;
		if (pos < 2)
			pos = 9;
	}
	resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
	if (resultado != digitos.charAt(1))
		return false;

	return true;
};

var loading = function () {
	$(document).ajaxStart(function () {
		$('#app-loading').addClass('active');
	});

	$(document).ajaxStop(function (event, request, settings) {
		$('#app-loading').removeClass('active');
	});
};

var format_real = function (int) {
	var tmp = int + '';
	tmp = tmp.replace(/([0-9]{2})$/g, ",$1");
	if (tmp.length > 6)
		tmp = tmp.replace(/([0-9]{3}),([0-9]{2}$)/g, ".$1,$2");

	return tmp;
};

var geral = (function () {
	var modal = {
		close: function () {
			$('.modal .close').on('click', function (event) {
				event.preventDefault();

				$('.modal, #overlay, body').removeClass('active');
			});
		}
	};

	modal.close();

	var ambos = {
		title: function () {
			$(window).load(function () {
				//TITLE
				$('.home main .full_banner .desktop ul .slick-slide a img').each(function (index, item) {
					$(item).parents('a').append($(item).attr('alt'));
				});
			});
		},

		avise_me: function () {
			$('.open_avise_me').on('click', function (e) {
				e.preventDefault();
				open_modal('.modal.avise_me');

				$('.modal.avise_me .success').fadeOut(300, function () {
					setTimeout(function () {
						$('.modal.avise_me .content').fadeIn(300);
					}, 300);
				});
			});

			$('#avise_me').submit(function (e) {
				e.preventDefault();

				let obj = {
					'nome': $('#avise_me input[name="nome"]').val(),
					'telefone': $('#avise_me input[name="telefone"]').val(),
					'email': $('#avise_me input[name="email"]').val()
				}

				let json = JSON.stringify(obj);

				insertMasterData('AV', 'ferrerofoodservice', json, function (res) {
					console.log(res);

					$('.modal.avise_me .content').fadeOut(300, function () {
						setTimeout(function () {
							$('.modal.avise_me .success').fadeIn(300);
						}, 300);
					});
				});
			});
		},

		distribuidores: function () {
			$('.open_distribuidores').on('click', function (e) {
				e.preventDefault();
				open_modal('.modal.distribuidores');
			});

			$.ajax({
					url: '/arquivos/lista-distribuidores.js',
					type: 'GET',
					dataType: 'json'
				})
				.done(function (res) {
					$.each(res, function (a, b) {
						let content = '';
						content += '<li>';
						content += '<div class="column column_1">';
						content += '<p>' + b.field2 + '</p>';
						content += '</div>';

						content += '<div class="column column_2">';
						content += '<p>' + b.field3 + '</p>';
						content += '</div>';


						content += '<div class="column column_3">';
						content += '<p>' + b.field4 + '</p>';
						content += '</div>';


						content += '<div class="column column_4">';
						content += '<p>' + b.field5 + '</p>';
						content += '</div>';


						content += '<div class="column column_5">';
						content += '<p>' + b.field6 + '</p>';
						content += '</div>';
						content += '</li>';

						$('.modal.distribuidores ul').append(content);
					});
				});
		},

		privacidade: function () {
			if (get_cookie('privacidade') != 'true') {
				open_modal('.modal.politicas, body');
			}

			$('.modal.politicas .footer ul .aceito').on('click', function (event) {
				event.preventDefault();

				set_cookie('privacidade', true, 7);
				$('.modal, #overlay, body').removeClass('active');
			});

			$('.modal.politicas .footer ul .cancelar').on('click', function (event) {
				event.preventDefault();

				open_modal('.modal.acesso.false, body');
			});
		},

		mask: function () {
			$('input[data-mask="cep"]').mask('00000-000');
			$('input[data-mask="cnpj"]').mask('99.999.999/9999-99');
			$('#create_account input[name="numero"]').mask('(00) 00000-0000');
		}
	};

	ambos.title();
	ambos.avise_me();
	ambos.distribuidores();
	ambos.privacidade();
	ambos.mask();
	qtd();

	var mobile = {
		hamburger: function () {
			$('header i.hamburger').on('click', function () {
				$('header nav, #overlay, body').addClass('active');
			});

			$('header nav .head .close').on('click', function (e) {
				e.preventDefault();
				$('header nav, #overlay, body').removeClass('active');
			});
		},

		slide_Toggle: function () {
			$('.send_card_fast').click(function (event) {
				$('.slide_infoFast').toggle(700);
			});

			$('.send_card_express').click(function (event) {
				$('.slide_infoExps').toggle(700);
			});
		}
	};

	if ($('body').width() < $mobile) {
		mobile.hamburger();
		mobile.slide_Toggle();
	}
})();

var produto = (function () {
	var desktop = {
		button: function () {
			$('#notifymeButtonOK').attr('value', 'Quero ser avisado !');
		},

		total_price: function (quantidade) {
			vtexjs.catalog.getCurrentProductWithVariations().done(function (product) {
				let math = product.skus[0].bestPrice * quantidade;
				$('.plugin-preco .skuBestPrice').text('R$ ' + format_real(math));
			});
		},

		estoque: function () {
			//VALOR INICIAL
			$(window).load(function () {
				$('.sku-selector-2').val('1').trigger('change');
			});

			$('.sku-selector-2').change(function (event) {
				let value = $(this).val();

				if ($('.notifyme').attr('style') === 'display: block;') {
					$('.plugin-preco').hide();
				} else {
					$('.plugin-preco').show();
				}

				//MENSAGEM DE VALOR UNITARIO
				if (value != '1') {
					$('.plugin-preco').addClass('no-message');
				} else {
					$('.plugin-preco').removeClass('no-message');
				}

				desktop.total_price(parseInt(value));
				$('.buy-button').attr('data-quantity', value);
			});
		},

		buy: function () {
			$('.buy-button').on('click', function (event) {
				event.preventDefault();

				$(this).addClass('disabled');

				let id = skuJson.skus[0].sku;
				let qtd = $(this).data('quantity');

				var item = {
					id: id,
					quantity: qtd,
					seller: '1'
				};

				//REMOVE ITEMS DO CARRINHO
				vtexjs.checkout.removeAllItems()
					.done(function (orderForm) {
						console.log('Carrinho esvaziado.');

						//ADD NO CARRINHO
						vtexjs.checkout.getOrderForm()
							.done(function (orderForm) {
								vtexjs.checkout.getOrderForm()
								vtexjs.checkout.addToCart([item], null, 1).done(function (orderForm) {
									window.location.href = '/checkout';
								});
							});
					});
			});
		}
	};

	desktop.button();
	desktop.estoque();
	desktop.buy();
})();

var account = (function () {
	var ambos = {
		politicas: function () {
			let userId = get_cookie('IPI').split('&')[0].split('UsuarioGUID=')[1];

			$.ajax({
				type: 'GET',
				url: '/api/dataentities/CL/search?_fields=id,politicaPrivacidade,politicaUso,termosUso&userId=' + userId,
				headers: header
			}).done(function (res, status) {
				console.log(res);

				$('#account_info').attr('data-id', res[0].id);

				$.each(res[0], function (key, value) {
					$('#account_info i[data-name="' + key + '"]').attr('data-value', value);
				});
			});

			$('#account_info ul li p i:not(.disabled)').on('click', function () {
				//ADD LOADING
				$('#app-loading').addClass('active');

				if ($(this).attr('data-value') === 'true') {
					$(this).attr('data-value', 'false');

					var obj = {
						'politicaPrivacidade': false
					};
				} else {
					$(this).attr('data-value', 'true');

					var obj = {
						'politicaPrivacidade': true
					};
				}

				let json = JSON.stringify(obj);

				$.ajax({
					type: 'PATCH',
					url: '/api/dataentities/CL/documents/' + $('#account_info').attr('data-id'),
					data: json,
					headers: header
				}).done(function (res, status) {
					//REMOVE LOADING
					$('#app-loading').removeClass('active');
				}).fail(function (res, status) {
					//REMOVE LOADING
					$('#app-loading').removeClass('active');

					swal("Oops!", "Houve um problema. Tente novamente mais tarde.", "error");
				});
			});
		}
	};

	if ($('body').hasClass('account')) {
		ambos.politicas();

		$(window).load(function () {
			$('#app-loading').removeClass('active')
		});
	}
})();

var login = (function () {
	var ambos = {
		logout: function () {
			//REMOVE EMAIL DO CHECKOUT - CHANGE userProfileId
			$('header .btn.logout').on('click', function (event) {
				event.preventDefault();

				vtexjs.checkout.getOrderForm()
					.then(function (orderForm) {
						var logoutURL = vtexjs.checkout.getLogoutURL();

						$.ajax({
							url: logoutURL,
							type: 'GET'
						}).done(function (res, status) {
							window.location = '/login';
						});
					});
			});
		},

		status: function () {
			$.ajax({
				url: '/api/vtexid/pub/authenticated/user',
				type: 'GET'
			}).done(function (user) {
				console.log(user);

				//SUPORTE = NÃO ESTÁ LOGADO
				if (user === null || user.user === 'suporte@sandersdigital.com.br') {
					console.log('Cliente: Deslogado.');

					//REMOVE ITENS DO CARRINHO
					vtexjs.checkout.removeAllItems()
						.done(function (orderForm) {
							console.log('Carrinho vazio.');
							qtd();
						});
				} else {
					console.log('Cliente: Logado.');

					//HOME - CEP (AVISO)
					$('#home_cep').addClass('disabled');

					//MENU DE APOIO - HEADER
					$('header .row_2 .columns').addClass('active'); //MOBILE
					$('header .btn.logout, header .btn.account, header .btn.pedidos, header .menu a[title="Quero Comprar"]').removeClass('dis-none');
					$('header .btn.entrar, .btn.cadastrar').addClass('dis-none');

					//LINK BANNERS
					$('.full_banner a[href="/login"]').attr('href', '/nutella-3kg/p');

					//PÁGINA DE PRODUTO
					$('.produto .buy-button.dis-none').removeClass('dis-none');
					$('.produto .list.account').addClass('dis-none');

					$.ajax({
						type: 'GET',
						url: "/api/dataentities/CL/search?_fields=firstName,lastName,document,phone,corporateName,tradeName,stateRegistration,corporateDocument,politicaPrivacidade&email=" + user.user,
						headers: header
					}).done(function (res) {
						if (res.length != 0) {
							//CLIENT NAME
							$('header a[title="Minha Conta"]').text('Olá, ' + res[0].firstName + '!'); //DESKTOP
							$('header nav > .head a[href="/account"]').append('<span>Olá, ' + res[0].firstName + '!</span>'); //MOBILE

							//PF
							let firstName = res[0].firstName;
							let lastName = res[0].lastName;
							let document = res[0].document;
							let phone = res[0].phone;

							//PJ
							let stateRegistration = res[0].stateRegistration != undefined ? res[0].stateRegistration : 'Não informado.';
							let corporateDocument = res[0].corporateDocument != undefined ? res[0].corporateDocument : 'Não informado';
							let politicaPrivacidade = res[0].politicaPrivacidade;

							if (res[0].tradeName === '' || res[0].tradeName === null) {
								var corporateName = res[0].corporateName;
								var tradeName = res[0].corporateName;
							} else {
								var corporateName = res[0].corporateName; //RAZÃO SOCIAL			
								var tradeName = res[0].tradeName; //FANTASIA
							}

							//INFORMAÇÕES DA EMPRESA
							vtexjs.checkout.getOrderForm().then(function (orderForm) {
								var clientProfileData = orderForm.clientProfileData;

								//PF
								clientProfileData.firstName = firstName;
								clientProfileData.lastName = lastName;
								clientProfileData.document = document;
								clientProfileData.documentType = 'cpf';
								clientProfileData.phone = phone;

								//PJ
								clientProfileData.isCorporate = true;
								clientProfileData.corporateName = corporateName;
								clientProfileData.tradeName = tradeName;
								clientProfileData.stateInscription = stateRegistration;
								clientProfileData.corporateDocument = corporateDocument;

								return vtexjs.checkout.sendAttachment('clientProfileData', clientProfileData);
							}).done(function (orderForm) {
								console.log('clientProfileData: Atualizado.');
							});

							//NEWSLETTER?
							vtexjs.checkout.getOrderForm().then(function (orderForm) {
								var clientPreferencesData = orderForm.clientPreferencesData;

								clientPreferencesData.optinNewsLetter = politicaPrivacidade;

								return vtexjs.checkout.sendAttachment('clientPreferencesData', clientPreferencesData);
							}).done(function (orderForm) {
								console.log('clientPreferencesData: Atualizado.');
							});
						} else {
							//ENTIDADE CL VAZIA
						}
					});
				}
			}).fail(function (user) {
				console.log('Ocorreu um erro, tente mais tarde.');
			});
		},

		//recebe email e senha
		request: function (email, senha) {
			$.ajax({
				url: '/api/vtexid/pub/authentication/start?callbackUrl=_secure%2Faccount%2Forders%2F&scope=ferrerofoodservice',
				type: 'GET',
				headers: header
			}).done(function (response) {
				$.ajax({
					url: '/api/vtexid/pub/authentication/classic/validate?authenticationToken=' + response.authenticationToken + '&login=' + email + '&password=' + senha,
					type: 'POST'
				}).done(function (res) {

					if (res.authStatus == 'WrongCredentials') {
						swal("Oops", "Senha inválida", "error");
					} else if (res.authStatus = 'Success') {
						window.location = '/nutella-3kg/p';
					}

					//HABILITA BOTÃO
					$('#entrar input[type="submit"]').val('Entrar');
				}).fail(function (res) {
					//HABILITA BOTÃO
					$('#entrar input[type="submit"]').val('Entrar');
					swal("Oops", "Senha inválida", "error");
				});
			});
		},

		entrar: function () {
			$("#entrar").submit(function (event) {
				event.preventDefault();

				$('#entrar input[type="submit"]').val('Aguarde...');

				let document = $('#entrar input[name="cnpj"]').val().replace(/\./g, "").replace(/\-/g, "").replace(/\//g, "");

				$.ajax({
					type: 'GET',
					headers: header,
					url: '/api/dataentities/CL/search?_fields=corporateDocument,email',
				}).done(function (res) {
					console.log(res);
					$.each(res, function (a, b) {
						if (b.corporateDocument === document) {
							$('#entrar input[name="email"]').attr('value', b.email);
						}
					});

					//email e senha
					setTimeout(function () {
						ambos.request($('#entrar input[name="email"]').val(), $('#entrar input[name="senha"]').val());
					}, 300);
				}).fail(function (res) {
					//HABILITA BOTÃO
					$('#entrar input[type="submit"]').val('Entrar');
					swal("Oops!", "Houve um problema. Tente novamente mais tarde.", "error");
				});
			});
		},

		esqueci: function () {
			$('.forgot_password').on('click', function (e) {
				e.preventDefault();

				//PRIMEIRAMENTE INFORME SEU EMAIl
				$('#send_token').addClass('recuperar');
				$('#send_token .description').text('Digite seu e-mail informado no seu cadastro.');
				$('.login .step_1').fadeOut(300, function () {
					setTimeout(function () {
						$('.login .step_2').fadeIn(300);
					}, 300);
				});
			});
		},

		change_password: function () {
			$('#change_password').submit(function (event) {
				event.preventDefault();

				let senha_1 = $('#change_password input[name="password_1"]').val();
				let senha_2 = $('#change_password input[name="password_2"]').val();

				let boleano = false;
				switch (boleano) {
					case $('.validate_password[data-name="caracter"]').hasClass('active'):
						swal('Oops', 'Forneça no mínimo 8 caracteres!', 'warning');
						break;
					case $('.validate_password[data-name="number"]').hasClass('active'):
						swal('Oops', 'Forneça ao menos 1 número!', 'warning');
						break;
					case $('.validate_password[data-name="uppercase"]').hasClass('active'):
						swal('Oops', 'Forneça ao menos 1 letra maiúscula!', 'warning');
						break;
					case $('.validate_password[data-name="lowercase"]').hasClass('active'):
						swal('Oops', 'Forneça ao menos 1 letra minúscula!', 'warning');
						break;
					case senha_1 === senha_2:
						swal('Oops', 'Revise sua senha!', 'warning');
						break;
					default:
						//LIBERADO
						let email_login = $('#send_token input[name="email"]').val();
						let senha_login = $('#change_password input[name="password_1"]').val();
						let autentica_login = $('#change_password input[name="token"]').val();
		
						var settings = {
							"async": true,
							"crossDomain": true,
							"url": "/api/vtexid/pub/authentication/start?callbackUrl=_secure%2Faccount%2Forders%2F&scope=ferrerofoodservice",
							"method": "GET"
						}
		
						$.ajax(settings).done(function (response) {
							var entrada = {
								"async": true,
								"crossDomain": true,
								"url": "/api/vtexid/pub/authentication/classic/setpassword?authenticationToken=" + response.authenticationToken + "&newPassword=" + senha_login + "&login=" + email_login + "&accessKey=" + autentica_login,
								"type": "POST"
							}
		
							$.ajax(entrada).done(function (response) {
								if (response.authStatus == 'WrongCredentials') {
									swal('Oops', 'Token inválido ou expirado!', 'error');
								} else {
									open_modal('.modal.senha.true');
								}
							});
						});
				}
			});
		}
	};

	ambos.logout();
	ambos.status();
	ambos.entrar();
	ambos.esqueci();
	ambos.change_password();
})();

var cadastro = (function () {
	var ambos = {
		preenchido: function () {
			$('form input:not([name="cpf"]), form input:not([name="cpf"]), form select').change(function () {
				$(this).addClass('success');
			});
		},

		validate_password: function () {
			$('#active_account input[name="password_1"], #change_password input[name="password_1"]').on('input', function (a, e) {
				let element = $(this).val();
				var upperCase = new RegExp('[A-Z]');
				var lowerCase = new RegExp('[a-z]');
				var numbers = new RegExp('[0-9]');

				if (element != '') {
					//min length: 8
					if (element.length >= 8) {
						$('.validate_password[data-name="caracter"]').addClass('active');
					} else {
						$('.validate_password[data-name="caracter"]').removeClass('active');
					}

					//is number
					if ($(this).val().match(numbers) != null) {
						$('.validate_password[data-name="number"]').addClass('active');
					} else {
						$('.validate_password[data-name="number"]').removeClass('active');
					}

					//is lowercase
					if ($(this).val().match(lowerCase) != null) {
						$('.validate_password[data-name="lowercase"]').addClass('active');
					} else {
						$('.validate_password[data-name="lowercase"]').removeClass('active');
					}

					//is uppercase
					if ($(this).val().match(upperCase) != null) {
						$('.validate_password[data-name="uppercase"]').addClass('active');
					} else {
						$('.validate_password[data-name="uppercase"]').removeClass('active');
					}
				} else {
					$('.validate_password').removeClass('active');
				}
			});
		},

		consultar_cep: function () {
			//CEP JÁ FOI PREENCHIDO NA HOME?
			if (get_cookie('cep')) {
				//INFORME SEU CNPJ
				$('.content_1 .step_1').fadeOut(300, function () {
					setTimeout(function () {
						$('.content_1 .step_2').fadeIn(300);
					}, 300);
				});
			}

			$("#consultar_cep").submit(function (event) {
				event.preventDefault();

				if ($("#consultar_cep input[name='cep']").val().length === 9) {

					$('#consultar_cep input[type="submit"]').val('Aguarde...');

					var items = [{
						id: 5,
						quantity: 1,
						seller: '1'
					}];

					var postalCode = $(this).find('input[name="cep"]').val();

					vtexjs.checkout.simulateShipping(items, postalCode, 'BRA')
						.done(function (result) {

							$('#consultar_cep input[type="submit"]').val('Continuar');

							if (result.messages.length != 0) {
								let cannotBeDelivered = result.messages[0].code === 'cannotBeDelivered';

								//ATENDEMOS ESSE CEP?
								if (cannotBeDelivered === true) {
									open_modal('.modal.cep.false');
								}
							} else {
								open_modal('.modal.cep:not(.home).true');

								//ATUALIZA CEP NO CHECKOUT
								vtexjs.checkout.getOrderForm()
									.then(function (orderForm) {
										var address = {
											"postalCode": postalCode,
											"country": 'BRA'
										};
										return vtexjs.checkout.calculateShipping(address)
									})
									.done(function (orderForm) {
										console.log('CEP atualizado no Checkout.');
									});

								//PRIMEIRAMENTE INFORME SEU CNPJ
								$('.content_1 .step_1').fadeOut(300, function () {
									setTimeout(function () {
										$('.content_1 .step_2').fadeIn(300);
									}, 300);
								});
							}
						});
				} else if ($("#consultar_cep input[name='cep']").val().length === 0) {
					swal("Oops!", "Digite seu CEP!", "warning");
				} else {
					swal("Oops!", "CEP Inválido!", "error");
				}
			});
		},

		consultar_cnpj: function () {
			$("#consultar_cnpj").submit(function (event) {
				event.preventDefault();

				let input_cnpj = $(this).find('input[name="cnpj"]').val();
				let regex = input_cnpj.replace(/\./g, "").replace(/\-/g, "").replace(/\//g, "");

				$('#consultar_cnpj input[type="submit"]').val('Aguarde...');

				$.ajax({
					url: 'https://clubenutella.ferrerofoodservice.com.br/vtex/api/connect_api.php',
					dataType: 'json',
					method: 'GET',
					action: 'GET',
					data: {
						parametro: $site + "/api/dataentities/CL/search?corporateDocument=" + regex
					}
				}).
				done(function (response) {
					if (response.length === 0) {
						$.ajax({
							method: "POST",
							type: "POST",
							dataType: "json",
							url: "https://clubenutella.ferrerofoodservice.com.br/vtex/api/consulta_cnpj.php",
							data: {
								"document": input_cnpj
							},
							success: function (res) {
								console.log(res);
								
								if (res.Mensagem === 'Documento invalido!') {
									swal("Oops", "CNPJ inválido!", "error");

									$('#consultar_cnpj input[type="submit"]').val('Pronto');
								} else if (res.Ativo === false) {
									swal("Oops", "CNPJ inativo!", "error");

									$('#consultar_cnpj input[type="submit"]').val('Pronto');
								} else {
									//LISTA DE CNAES PERMITIDOS
									var list_cnae = ['4729-6/99', '4639-7/01', '1053-8/00', '1082-1/00', '1091-1/01', '1091-1/02', '1092-9/00', '1093-7/01', '1093-7/02', '1094-5/00', '1096-1/00', '4617-6/00', '4631-1/00', '4632-0/02', '4632-0/03', '4637-1/01', '4637-1/04', '4637-1/06', '4637-1/07', '4711-3/01', '4711-3/02', '4712-1/00', '4721-1/02', '4721-1/03', '4721-1/04', '4729-6/02', '5510-8/01', '5510-8/02', '5510-8/03', '5590-6/01', '5590-6/02', '5590-6/03', '5590-6/99', '5611-2/01', '5611-2/03', '5611-2/04', '5611-2/05', '5612-1/00', '5620-1/01', '5620-1/02', '5620-1/03', '5620-1/04', '7490-1/05', '7911-2/00', '7912-1/00', '7990-2/00', '8230-0/01', '8230-0/02', '8411-6/00', '8412-4/00', '8413-2/00', '8421-3/00', '8422-1/00', '8423-0/00', '8424-8/00', '8425-6/00', '8430-2/00', '8513-9/00', '8520-1/00', '8531-7/00', '8532-5/00', '8533-3/00', '8541-4/00', '8542-2/00', '8550-3/01', '8550-3/02', '8591-1/00', '8592-9/01', '8592-9/02', '8592-9/03', '8592-9/99', '8593-7/00', '8599-6/01', '8599-6/02', '8599-6/03', '8599-6/04', '8599-6/05', '8599-6/99', '8711-5/01', '8711-5/02', '8711-5/03', '8711-5/04', '8711-5/05', '8712-3/00', '8720-4/01', '8720-4/99', '8730-1/01', '8730-1/02', '9001-9/01', '9001-9/02', '9001-9/03', '9001-9/04', '9001-9/05', '9001-9/06', '9001-9/99', '9002-7/01', '9003-5/00', '9312-3/00', '9319-1/01', '9321-2/00', '9411-1/00', '9412-0/01', '9412-0/99', '9420-1/00', '9430-8/00', '9491-0/00', '9492-8/00', '9493-6/00', '9499-5/00'],
										isMatch = [];

									//VALIDANDO CNAE PRINCIPAL
									var cae = res.CodigoAtividadeEconomica.replace('.', '');
									for (var i = 0; i < list_cnae.length; i++) {
										let element = list_cnae[i];
										if (element === cae) {
											//PERMITIDO
											isMatch.push(element);
										}
									}

									//VALIDANDO CNAES SECUNDARIOS, CASO HOUVER
									if (res.CNAES.length != 0) {
										$(res.CNAES).each(function (a, el) {
											$(list_cnae).each(function (b, cnae) {
												if (el.CNAE.replace('.', '') === cnae) {
													//PERMITIDO
													isMatch.push(cnae);
												}
											});
										});
									}

									//VALIDANDO INSCRIÇÃO ESTADUAL
									if (res.InscricaoEstadual === '0' || res.InscricaoEstadual === '' || res.InscricaoEstadual === 0) {
										var ie = 'Isento';
									} else {
										var ie = res.InscricaoEstadual;
									}

									//AQUI, DEIXAR COM ===
									if (isMatch.length === 0) {
										//USUÁRIO NÃO POSSUI CNAE DA LISTA
										open_modal('.modal.cnpj.false');
										$('#consultar_cnpj input[type="submit"]').val('Pronto');
									} else {
										//USUÁRIO POSSUI CNAE DA LISTA

										//CEP DO CNPJ NÃO ATENDIDO?
										let items = [{
											id: 5,
											quantity: 1,
											seller: '1'
										}];

										function consultaCEP(cep) {
											vtexjs.checkout.simulateShipping(items, cep, 'BRA')
												.done(function (result) {
													//SIM ATENDIDO
													//AQUI, DEIXAR COM ===
													if (result.messages.length === 0) {
														//MINHA EMPRESA
														$('#create_account input[name="ie"]').val(ie);
														$('#create_account input[name="cnpj"]').val(res.Documento);
														$('#create_account input[name="razao-social"]').val(res.RazaoSocial);
														$('#create_account input[name="nome-fantasia"]').val(res.NomeFantasia);
														$('#create_account input[name="email-nfe"]').val();
														$('#create_account input[name="telefone"]').val(res.Telefones.length ? res.Telefones[0].Numero : "Não informado.");
														$('#create_account select[name="ramo-atividade"] option:selected').val();

														//MEU PERFIL
														$('#create_account input[name="nome"]').val();
														$('#create_account input[name="cpf"]').val();
														$('#create_account input[name="email"]').val();
														$('#create_account input[name="numero"]').val();
														$('#create_account input[name="relacao-empresa"]').val();

														//TERMOS
														$('#create_account input[name="termos-uso"]').val();
														$('#create_account input[name="politica-uso"]').val();
														$('#create_account input[name="politica-privacidade"]').val();

														//INFORMAÇÕES DO CNPJ
														$('.content_1').fadeOut(300, function () {
															setTimeout(function () {
																$('.content_2').fadeIn(300);
															}, 300);
														});

														$('#consultar_cnpj input[type="submit"]').val('Pronto');
													} else {
														//NÃO ATENDIDO
														let cannotBeDelivered = result.messages[0].code === 'cannotBeDelivered';

														if (cannotBeDelivered === true) {
															open_modal('.modal.cep.false');

															$('#consultar_cnpj input[type="submit"]').val('Pronto');
														}
													}
												});
										}

										//POSSUI ENDEREÇOS CADASTRADOS?
										if (res.Enderecos.length) {
											consultaCEP(res.Enderecos[0].CEP);
										} else {
											consultaCEP(get_cookie('cookie'));
										}
									}
								}
							},
							error: function () {
								swal("Oops", "Algo deu errado!", "error");

								//REMOVE - AGUARDE
								$('#consultar_cnpj input[type="submit"]').val('Pronto');
							}
						});
					} else {
						swal("Oops", "CNPJ já cadastrado!", "warning");

						$('#consultar_cnpj input[type="submit"]').val('Pronto');
					}
				}).fail(function (response) {
					console.log(response);
				});
			});

			//BACK
			$('#consultar_cnpj .back').on('click', function (event) {
				event.preventDefault();

				$('.content_1 .step_2').fadeOut(300, function () {
					setTimeout(function () {
						$('.content_1 .step_1').fadeIn(300);
					}, 300);
				});
			});
		},

		//ENVIA TOKEN PARA O E-MAIL
		send_token: function () {
			$('#send_token').submit(function (event) {
				event.preventDefault();

				$('#send_token input[type="submit"]').val('Aguarde...');

				//ENVIA TOKEN PARA EMAIL
				let email = $('#send_token input[name="email"]').val();

				function send() {
					$.ajax({
						type: 'GET',
						headers: header,
						url: '/api/vtexid/pub/authentication/start?callbackUrl=_secure%2Faccount%2Forders%2F&scope=ferrerofoodservice'
					}).
					done(function (start) {
						let token = start.authenticationToken;
						localStorage.setItem('token', start.authenticationToken);

						$.ajax({
								type: 'POST',
								url: "/api/vtexid/pub/authentication/accesskey/send?authenticationToken=" + token + "&email=" + email
							})
							.done(function (response) {
								//PREENCHE O CAMPO EMAIL DO FORM 2
								$('#active_account input[name="email"], #create_account input[name="email"], #create_account input[name="email-nfe"]').attr('value', email);

								//REMOVE AGUARDE
								$('#send_token input[type="submit"]').val('Próximo passo');

								//VAMOS ATIVAR SUA CONTA?
								$('.content_1 .step_3').fadeOut(300, function () {
									setTimeout(function () {
										$('.content_1 .step_4').fadeIn(300);
									}, 300);
								});

								//RECUPERANDO A SENHA(/LOGIN)
								$('body.login #change_password input[name="email"]').val('value', email);

								$('body.login .step_2').fadeOut(300, function () {
									setTimeout(function () {
										$('body.login .step_3').fadeIn(300);
									}, 300);
								});
							});
					});
				};

				//RECUPERAR SENHA
				if ($('#send_token').hasClass('recuperar')) {
					//EMAIL JÁ CADASTRADO?
					$.ajax({
						type: 'GET',
						headers: header,
						url: '/api/dataentities/CL/search?_fields=email&email=' + email
					}).done(function (res) {
						//REMOVE AGUARDE
						$('#send_token input[type="submit"]').val('Próximo passo');

						if (res.length === 0) {
							swal('Oops', 'E-mail informado não cadastrado!', 'warning');
						} else {
							send();
						}
					});
				} else {
					//CADASTRO
					send();
				}
			});
		},

		//VALIDA A CHAVE DE ACESSO E CRIA SENHA
		active_account: function () {
			$('#active_account').submit(function (event) {
				event.preventDefault();

				$('#active_account input[type="submit"]').val('Aguarde...');

				var senha_1 = $('#active_account input[name="password_1"]').val();
				var senha_2 = $('#active_account input[name="password_2"]').val();

				let boleano = false;
				switch (boleano) {
					case $('.validate_password[data-name="caracter"]').hasClass('active'):
						swal('Oops', 'Forneça no mínimo 8 caracteres!', 'warning');
						$('#active_account input[type="submit"]').val('Próximo Passo');
						break;
					case $('.validate_password[data-name="number"]').hasClass('active'):
						swal('Oops', 'Forneça ao menos 1 número!', 'warning');
						$('#active_account input[type="submit"]').val('Próximo Passo');
						break;
					case $('.validate_password[data-name="uppercase"]').hasClass('active'):
						swal('Oops', 'Forneça ao menos 1 letra maiúscula!', 'warning');
						$('#active_account input[type="submit"]').val('Próximo Passo');
						break;
					case $('.validate_password[data-name="lowercase"]').hasClass('active'):
						swal('Oops', 'Forneça ao menos 1 letra minúscula!', 'warning');
						$('#active_account input[type="submit"]').val('Próximo Passo');
						break;
					case senha_1 === senha_2:
						swal('Oops', 'Revise sua senha!', 'warning');
						$('#active_account input[type="submit"]').val('Próximo Passo');
						break;
					default:
						//LIBERADO
						var email_login = $('#active_account input[name="email"]').val();
						var senha_login = $('#active_account input[name="password_1"]').val();
						var autentica_login = $('#active_account input[name="token"]').val();
		
						var settings = {
							"async": true,
							"crossDomain": true,
							"url": "/api/vtexid/pub/authentication/start?callbackUrl=_secure%2Faccount%2Forders%2F&scope=ferrerofoodservice",
							"method": "GET"
						}
		
						$.ajax(settings).done(function (response) {
							var entrada = {
								"async": true,
								"crossDomain": true,
								"url": "/api/vtexid/pub/authentication/classic/setpassword?authenticationToken=" + localStorage.getItem('token') + "&newPassword=" + senha_login + "&login=" + email_login + "&accessKey=" + autentica_login,
								"type": "POST"
							}
		
							$.ajax(entrada).done(function (response) {		
								if (response.authStatus == 'WrongCredentials') {
									swal('Oops', 'Token inválido ou expirado!', 'error');
									$('#active_account input[type="submit"]').val('Próximo Passo');
								} else {
									//USUARIO CRIADO - SALVE INFORMAÇÕES NO MASTER DATA
									function saveUser () {
										var obj_cliente = {
											'isCorporate': true,
											'stateRegistration': $('#create_account input[name="ie"]').val(),
											'corporateDocument': $('#create_account input[name="cnpj"]').val(),
											'corporateName': $('#create_account input[name="razao-social"]').val(),
											'tradeName': $('#create_account input[name="nome-fantasia"]').val(),
											'emailNfe': $('#create_account input[name="email-nfe"]').val(),
											'businessPhone': $('#create_account input[name="telefone"]').val(),
											'ramoAtividade': $('#create_account select[name="ramo-atividade"] option:selected').val(),
											'firstName': $('#create_account input[name="nome"]').val(),
											'lastName': $('#create_account input[name="sobrenome"]').val(),
											'document': $('#create_account input[name="cpf"]').val(),
											'email': $('#create_account input[name="email"]').val(),
											'phone': $('#create_account input[name="numero"]').val(),
											'relacaoEmpresa': $('#create_account select[name="relacao-empresa"] option:selected').val(),
											'termosUso': $('#create_account input[name="termo-de-uso"]').is(':checked'),
											'politicaUso': $('#create_account input[name="politica-de-uso"]').is(':checked'),
											'politicaPrivacidade': $('#create_account input[name="politica-de-privacidade"]').is(':checked')
										}
					
										var json_cliente = JSON.stringify(obj_cliente);
					
										insertMasterData('CL', 'ferrerofoodservice', json_cliente, function (res) {
											console.log(res);
					
											//BLOCK SUBMIT
											$('#active_account input[type="submit"]').val('Cadastrado');
					
											//MESSAGE
											$('.modal.cadastro.true').addClass('active');
										});
									};
									saveUser();
								}
							}).fail(function () {		
								swal('Oops', 'Token inválido ou expirado!', 'error');
								$('#active_account input[type="submit"]').val('Próximo Passo');
							});
						});
				}
			});

			//BACK
			$('#active_account .back').on('click', function (event) {
				event.preventDefault();

				$('.content_1 .step_4').fadeOut(300, function () {
					setTimeout(function () {
						$('.content_1 .step_3').fadeIn(300);
					}, 300);
				});
			});
		},

		validate_cpf: function () {
			function cpf(strCPF) {
				var Soma;
				var Resto;
				Soma = 0;
				if (
					strCPF == "00000000000" ||
					strCPF == "11111111111" ||
					strCPF == "22222222222" ||
					strCPF == "33333333333" ||
					strCPF == "44444444444" ||
					strCPF == "55555555555" ||
					strCPF == "66666666666" ||
					strCPF == "77777777777" ||
					strCPF == "88888888888" ||
					strCPF == "99999999999"
				) return false;

				for (i = 1; i <= 9; i++) Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i);
				Resto = (Soma * 10) % 11;

				if ((Resto == 10) || (Resto == 11)) Resto = 0;
				if (Resto != parseInt(strCPF.substring(9, 10))) return false;

				Soma = 0;
				for (i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (12 - i);
				Resto = (Soma * 10) % 11;

				if ((Resto == 10) || (Resto == 11)) Resto = 0;
				if (Resto != parseInt(strCPF.substring(10, 11))) return false;
				return true;
			}

			$('#create_account input[name="cpf"]').keyup(function (e) {
				if (/\D/g.test(this.value)) {
					this.value = this.value.replace(/\D/g, '');
				}
			});

			$('#create_account input[name="cpf"]').focusout(function () {
				if ($(this).val().length > 11) {
					$('#create_account input[name="cpf"]').addClass('alert');
				} else {
					if (cpf($(this).val()) === true) {
						$('#create_account input[name="cpf"]').removeClass('alert');
					} else {
						$('#create_account input[name="cpf"]').addClass('alert');
					}
				}
			});
		},

		create_account: function () {
			//CADASTRAR INFORMAÇÕES DO CLIENTE
			$('#create_account').submit(function (event) {
				event.preventDefault();

				if ($('#create_account input[name="cpf"]').hasClass('alert')) {
					swal("Oops!", "CPF inválido", "error");
				} else if ($('#create_account input[name="numero"]').val().length === 0) {
					swal("Oops!", "Preencha o telefone", "error");
				} else {
					//AUTO PREENCHE EMAIL - TOKEN
					$('#send_token input[type="email"]').val($('#create_account input[type="email"]').val());

					//TOKEN PARA O USUÁRIO
					$('.content_2, .content_1 .step.step_2').fadeOut(300, function () {
						setTimeout(function () {
							$('.content_1, .content_1 .step.step_3').fadeIn(300);
						}, 300);
					});
				}
			});

			//NEXT STEP
			$('#create_account .next_step').on('click', function (event) {
				event.preventDefault();

				$('#create_account fieldset[data-step="1"]').fadeOut(300, function () {
					$('.cadastro main section.content.content_2 ul li[data-step="1"]').removeClass('active');

					setTimeout(function () {
						$('.cadastro main section.content.content_2 ul li[data-step="2"]').addClass('active');

						$('#create_account fieldset[data-step="2"]').fadeIn(300);
					}, 300);
				});
			});

			//BACK STEP
			$('#create_account .submit > a').on('click', function (event) {
				event.preventDefault();

				$('#create_account fieldset[data-step="2"]').fadeOut(300, function () {
					$('.cadastro main section.content.content_2 ul li[data-step="2"]').removeClass('active');

					setTimeout(function () {
						$('.cadastro main section.content.content_2 ul li[data-step="1"]').addClass('active');

						$('#create_account fieldset[data-step="1"]').fadeIn(300);
					}, 300);
				});
			});
		},
	};

	//USADAS EM OUTRAS PÁGINAS - LOGIN
	ambos.send_token();
	ambos.validate_password();

	if ($('body').hasClass('cadastro')) {
		ambos.preenchido();
		ambos.consultar_cep();
		ambos.consultar_cnpj();
		ambos.active_account();
		ambos.validate_cpf();
		ambos.create_account();
	}
})();

var home = (function () {
	var ambos = {
		blank: function () {
			$('.full_banner img[alt="Clube Nutella"]').parent('a').attr('target', '_blank');
		},

		cep: function () {
			$("#home_cep").submit(function (event) {
				event.preventDefault();

				if ($('#home_cep').hasClass('disabled')) {
					//VOCÊ JÁ ESTÁ ONLINE
					open_modal('.modal.online');
				} else {
					//CONSULTAR CEP
					if ($("#home_cep input[name='cep']").val().length === 9) {

						$('#home_cep input[type="submit"]').val('Aguarde...');

						var items = [{
							id: 5,
							quantity: 1,
							seller: '1'
						}];

						var postalCode = $(this).find('input[name="cep"]').val();

						vtexjs.checkout.simulateShipping(items, postalCode, 'BRA')
							.done(function (result) {
								console.log(result);

								$('#home_cep input[type="submit"]').val('Consultar');

								//ATENDEMOS ESSE CEP?
								if (result.messages.length === 0) {
									open_modal('.modal.cep.home.true');

									set_cookie('cep', postalCode, 1);
								} else {
									let cannotBeDelivered = result.messages[0].code === 'cannotBeDelivered';

									if (cannotBeDelivered === true) {
										open_modal('.modal.cep.false');
									}
								}
							});
					} else if ($("#home_cep input[name='cep']").val().length === 0) {
						swal("Oops!", "Digite seu CEP!", "warning");
					} else {
						swal("Oops!", "CEP inválido", "error");
					}
				}
			});
		}
	};

	ambos.blank();
	ambos.cep();

	var desktop = {
		banner: function () {
			$('.full_banner ul').slick({
				autoplay: true,
				autoplaySpeed: 6500,
				infinite: false,
				arrows: false,
				dots: true,
				slidesToShow: 1,
				slidesToScroll: 1
			});
		}
	};

	var mobile = {
		tipbar: function () {
			$('.home .tipbar ul').slick({
				infinite: false,
				autoplay: true,
				autoplaySpeed: 4000,
				arrows: false,
				dots: false,
				slidesToShow: 1,
				slidesToScroll: 1
			});
		},

		toggle: function () {
			$('footer .toggle .title').on('click', function () {
				$(this).toggleClass('active');
				$(this).next('ul').slideToggle();
			});
		}
	};

	desktop.banner();

	if ($('body').width() < $mobile) {
		mobile.tipbar();
		mobile.toggle();
	}
})();

var institucional = (function () {
	var ambos = {
		contato: function () {
			function cnpj() {
				$('#contato input[name="cnpj"]').focusout(function () {
					if (validate_cnpj($(this).val()) === false) {
						$(this).addClass('alert');
					} else {
						$(this).removeClass('alert');
					}
				});
			};

			function cep() {
				loading(); //LOADING PARA REQUISIÇAO: VIACEP

				$('#contato input[name="cep"]').focusout(function () {
					$(this).removeClass('alert');

					if ($('#contato input[name="cep"]').val().length === 9) {
						$.ajax({
								type: 'GET',
								url: 'https://viacep.com.br/ws/' + $(this).val() + '/json/'
							})
							.done(function (res) {
								$('#contato input[name="numero"]').val('');
								$('#contato input[name="rua"]').val(res.logradouro != '' ? res.logradouro : 'Não encontrado.');
								$('#contato input[name="bairro"]').val(res.bairro != '' ? res.bairro : 'Não encontrado.');
								$('#contato input[name="cidade"]').val(res.localidade != '' ? res.localidade : 'Não encontrado.');
								$('#contato select[name="estado"] option[value="' + res.uf + '"]').attr('selected', 'selected');
							});
					} else if ($('#contato input[name="cep"]').val().length === 0) {
						$(this).addClass('alert');
					} else {
						$(this).addClass('alert');
					}
				});
			};

			function send() {
				$('#contato').submit(function (event) {
					event.preventDefault();

					if ($('#contato input[name="cnpj"]').hasClass('alert')) {
						swal('Oops!', 'CNPJ Inválido.', 'warning');
					} else {
						let obj = {
							'nome': $('#contato input[name="nome"]').val(),
							'nomeEmpresa': $('#contato input[name="nome-empresa"]').val(),
							'cnpj': $('#contato input[name="cnpj"]').val(),
							'telefone': $('#contato input[name="telefone"]').val(),
							'email': $('#contato input[name="email"]').val(),
							'cep': $('#contato input[name="cep"]').val(),
							'rua': $('#contato input[name="rua"]').val(),
							'numero': $('#contato input[name="numero"]').val(),
							'bairro': $('#contato input[name="bairro"]').val(),
							'cidade': $('#contato input[name="cidade"]').val(),
							'estado': $('#contato select option[selected="selected"]').val(),
							'mensagem': $('#contato textarea[name="mensagem"]').val()
						};

						let json = JSON.stringify(obj);

						insertMasterData('CT', 'ferrerofoodservice', json, function (res) {
							$('#contato').trigger('reset');
							$('#contato select[name="estado"] option:empty').attr('selected', 'selected');

							swal('Mensagem enviada.', 'Em breve, retornaremos o contato!', 'success');
						});
					}
				});
			};

			// cnpj();
			cep();
			send();
		},

		printer: function () {
			$('.institucional #btn-printer').on('click', function () {
				var conteudo = document.getElementById('printer').innerHTML;
				var win = window.open();
				win.document.write(conteudo);
				win.print();
				win.close();
			});
		}
	};

	var desktop = {
		title: function () {
			//P�GINA ATUAL
			let page = $('body').attr('class').split(' ')[1];
			$('.institucional .menu ul li a[data-link="' + page + '"]').addClass('active');
		},

		questions: function () {
			$('.institucional .questions .title').on('click', function () {
				$(this).toggleClass('active');
				$(this).next('.text').slideToggle();
			});
		}
	};

	var mobile = {
		menu: function () {
			$('.institucional .menu > .btn').on('click', function () {
				$(this).toggleClass('active');
				$(this).next('ul').slideToggle();
			});
		}
	};

	ambos.contato();
	ambos.printer();

	desktop.title();
	desktop.questions();

	if ($('body').width() < $mobile) {
		mobile.menu();
	}
})();