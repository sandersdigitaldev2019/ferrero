[{
        "field2": "AROUMAR DISTRIBUIDORA DE PRODUTOS A",
        "field3": "R. Freire Bastos, 430",
        "field4": "São Paulo",
        "field5": "SP",
        "field6": "http://www.aroumar.com.br/"
    },
    {
        "field2": "EBD - EMPRESA BRASILEIRA DE DISTRIBUICAO",
        "field3": "R. Góis Raposo, 1444",
        "field4": "São Paulo",
        "field5": "SP",
        "field6": "http://www.ebdgrupo.com.br/"
    },
    {
        "field2": "INTERFRIOS COMERCIO DE FRIOS",
        "field3": "Avenida. Professor Papini, 213",
        "field4": "São Paulo",
        "field5": "SP",
        "field6": "http://www.montecarloalimentos.com.br"
    },
    {
        "field2": "DISTRIBUIDORA E IMPORTADORA IRMAOS",
        "field3": "Av. Marginal Do Ribeirao, 4633",
        "field4": "Carapicuiba",
        "field5": "SP",
        "field6": "http://www.iavelino.com.br/"
    },
    {
        "field2": "MARIUSSO COM DE ALIMEN E REPRES COM",
        "field3": "R. Joaquim Arico, 275",
        "field4": "Paulona",
        "field5": "SP",
        "field6": "http://www.mariusso.com.br/institucional/"
    },
    {
        "field2": "RIBERFOODS IMPORT E DISTRIB DE PROD",
        "field3": "Rua. Dante Ferezin 400, Lt Dis Ind.",
        "field4": "Ribeirão Preto",
        "field5": "SP",
        "field6": "http://www.riberdoces.com.br/novo/"
    },
    {
        "field2": "A PARO E CIA LTDA",
        "field3": "Rua Quinze de Novembro, 2710",
        "field4": "São José do Rio Preto",
        "field5": "SP",
        "field6": "http://casapaulista.com.br/"
    },
    {
        "field2": "PAMA COMERCIO DE GENEROS ALIMENTICI",
        "field3": "Av. Albert Einstein, 80",
        "field4": "Taboão da Serra",
        "field5": "SP",
        "field6": "http://www.pmg.com.br/"
    },
    {
        "field2": "NOVA MEGA G ATACADISTA DE",
        "field3": "Est. Ribeirao Das Lages, 50",
        "field4": "Vargem Grande Paulista",
        "field5": "SP",
        "field6": "http://megag.com.br/"
    },
    {
        "field2": "CEREALISTA NOVA SAFRA LIMITADA",
        "field3": "Via. Manoel Jacinto C 981, Galpao, 03",
        "field4": "Contagem",
        "field5": "MG",
        "field6": "https://www.novasafra.com.br/"
    },
    {
        "field2": "DISPROPAN LTDA",
        "field3": "R. Monsenhor Francisco De Paula, 150",
        "field4": "Juiz de Fora",
        "field5": "MG",
        "field6": "http://www.dispropan.com.br/"
    },
    {
        "field2": "DVL - DISTRIBUIDORA VIA LÁCTEA",
        "field3": "Av. Doutor Ângelo Teixeira Da Costa, 888",
        "field4": "Santa Luzia",
        "field5": "MG",
        "field6": "https://www.dvl.com.br/"
    },
    {
        "field2": "RIBERFOODS IMP E DIST DE PROD ALIME",
        "field3": "R. Ituiutaba, 200",
        "field4": "Uberaba",
        "field5": "MG",
        "field6": "http://www.riberdoces.com.br/novo/"
    },
    {
        "field2": "CCN - COMERCIAL CENTRO NORTE ALIMEN",
        "field3": "Av. Nossa Senhora Do Amparo S/N",
        "field4": "Nova Friburgo",
        "field5": "RJ",
        "field6": "https://www.novamixnf.com.br/"
    },
    {
        "field2": "EBD - EMPRESA BRASILEIRA DE DISTRIBUICAO",
        "field3": "R. Bispo Dom Mamemde, 77",
        "field4": "Rio de Janeiro",
        "field5": "RJ",
        "field6": "http://www.ebdgrupo.com.br/"
    },
    {
        "field2": "RIO QUALITY COMERCIO DE ALIMENTOS L",
        "field3": "R. Embau 2207, Lote, 01",
        "field4": "Rio de Janeiro",
        "field5": "RJ",
        "field6": "http://www.rioquality.com.br/"
    },
    {
        "field2": "COMERCIAL DISKPAN LTDA",
        "field3": "Av. Fernando Antonio, 1911",
        "field4": "Cariacica",
        "field5": "ES",
        "field6": "http://diskpan.com.br/"
    },
    {
        "field2": "DB DISTRIBUIDORA BRASIL DE ALIMENT",
        "field3": "Rod. Gleba 04 Rodovia DF 180 Lot, 500",
        "field4": "Brasilia",
        "field5": "DF",
        "field6": "http://www.dbbrasil.com/"
    },
    {

        "field2": "ARAUJO MATEUS IMPORTACAO E COMERCIO",
        "field3": "Av. Jequitaia, 1669",
        "field4": "Salvador",
        "field5": "BA",
        "field6": "http://araujomateus.com.br/"
    },
    {

        "field2": "FORTALI DISTRIBUIDORA DE ALIMENTOS",
        "field3": "Av. Jornalista Tomaz Coelho, 1268",
        "field4": "Fortaleza",
        "field5": "CE",
        "field6": "https://www.fortali.com.br/"
    },
    {

        "field2": "PADEIRAO COM DE PROD PARA PANIF LTD",
        "field3": "Rod. Br-101 - Sul S/N",
        "field4": "Jaboatão dos Guararapes",
        "field5": "PE",
        "field6": "http://www.padeirao.com/"
    },
    {

        "field2": "DAB - DISTRIBUIDORA DE ALIMENTOS E BEBIDAS",
        "field3": "Rodovia BR 304 KM296, n 922",
        "field4": "Natal",
        "field5": "RN",
        "field6": "http://www.dabrn.com.br/"
    },
    {
        "field2": "AGIL DISTRIBUIDORA DE ALIMENTOS LTD",
        "field3": "Av. Abel Dal'Bosco, 4490. Lt 14-19",
        "field4": "Sinop",
        "field5": "MT",
        "field6": "http://www.disnorteagil.com.br/"
    },
    {
        "field2": "POLINA COMERCIAL DE ALIMENTOS LTDA",
        "field3": "Av. Doutor Ezuel Portes, 17483",
        "field4": "Cascavel",
        "field5": "PR",
        "field6": "http://polinaalimentos.com.br/"
    },
    {
        "field2": "STAMPA FOOD DISTRIBUIDORA",
        "field3": "Al. Bom Pastor, 2426",
        "field4": "São Jose dos Pinhais",
        "field5": "PR",
        "field6": "https://stampafood.com.br/"
    },
    {
        "field2": "CIERS DISTRIBUIDORA",
        "field3": "R. Frederico Mentz, 483",
        "field4": "Porto Alegre",
        "field5": "RS",
        "field6": "https://www.ciers.com.br/"
    },
    {
        "field2": "BAIA NORTE PRODUTOS PARA RESTAUR LT",
        "field3": "R. Sebastiao Lara S/N, Galpao, 1",
        "field4": "Biguaçu",
        "field5": "SC",
        "field6": "http://bnfs.com.br/site/"
    },
    {
        "field2": "COMERCIO DE CARNES FINCO LTDA",
        "field3": "Rua. Prft Etelvino Pedro 443, Sala, 1",
        "field4": "Seara",
        "field5": "SC",
        "field6": "http://www.fincoalimentos.com.br/home/"
    }
]